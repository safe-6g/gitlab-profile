<div align="center">
![image info](img/safe6g_logo.png){width=40%}
</div>


SAFE-6G is a European research project (HORIZON-JU-SNS-2023-STREAM-B-01-04), which pioneers a holistic research approach, situating a native trustworthiness framework atop the open and distributed USN/NSN-based 6G core, leveraging (X)AI/ML techniques to coordinate user-centric safety, security, privacy, resilience, and reliability functions, strategically optimizing the Level of Trust (LoT) as a pivotal Key Value Indicator (KVI) for 6G, while addressing specific trust requirements and data governance policies specified by each user/tenant/human-role throughout the entire 6G lifecycle, encompassing onboarding, deployment, operation, and decomposition.

### Acknowledgement
<img src="https://safe-6g.eu/wp-content/uploads/2023/11/EUflagCoFunded6G-SNS_rgb_vertical-1.png" align="left" width="150"/>
SAFE-6G has received funding from the Smart Networks and Services Joint Undertaking (SNS JU) under the European Union’s Horizon Europe research and innovation programme under Grant Agreement No101139031.
<br clear="left"/>